import { TestBed } from '@angular/core/testing';
import { UrlApiService } from './url-api.service';

describe('UrlApiService', () => {
  let service: UrlApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UrlApiService);
  });

  it('Url Correctas', () => {
    expect(service.getUrlDescuentos()).toContain('wm-api/discount');
    expect(service.getUrlProductos()).toContain('wm-api/productos');
  });
});
